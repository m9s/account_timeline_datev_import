# This file is part of Tryton. The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
from trytond.model import ModelView, ModelSQL, fields


class MoveDatev(ModelSQL, ModelView):
    _name = 'account.move.datev'

    def _get_args_moves_export(self, datev_export, period_ids, journal_ids):
        args = super(MoveDatev, self)._get_args_moves_export(datev_export,
            period_ids, journal_ids)
        if not datev_export.include_imported_moves:
            args.append(('imported', '=', False))
        return args

MoveDatev()


class Move(ModelSQL, ModelView):
    _name = 'account.move'

    imported = fields.Boolean('Imported', readonly=True)

Move()
