# This file is part of Tryton. The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
import copy
import logging
from trytond.model import ModelView, ModelSQL, fields
from trytond.wizard import Wizard
from trytond.pyson import Equal, Eval, If, Bool, PYSONEncoder
from trytond.modules.account_import.account_import import _IMPORT_TYPES
from trytond.transaction import Transaction
from trytond.pool import Pool


HAS_CHARDET = False
try:
    import chardet
    HAS_CHARDET = True
except ImportError:
    logging.getLogger('account.banking.import').warning(
        'Unable to import chardet. Autodetection of file encoding disabled.')


if not ('datev', 'Datev') in _IMPORT_TYPES:
    _IMPORT_TYPES += [('datev', 'Datev')]


class ImportInit(ModelView):
    _name = 'account.import.init'

    def default_type(self):
        return 'datev'

ImportInit()


class ImportDatevInit(ModelView):
    'Import Datev Init'
    _name = 'account.import.datev.init'
    _description = __doc__

    company = fields.Many2One('company.company', 'Company', readonly=True)
    fiscalyear = fields.Many2One('account.fiscalyear', 'Fiscalyear',
            domain=[('company', '=', Eval('company'))], depends=['company'])
    period = fields.Many2One('account.period', 'Period',
            domain=[
                ('fiscalyear', If(Bool(Eval('fiscalyear')), '=', '!='),
                    Eval('fiscalyear')),
                ('fiscalyear.company', '=', Eval('company')),
            ], required=True, depends=['fiscalyear', 'company'])
    name_batch = fields.Char('Batch Name', required=True)
    import_file_cache = fields.Binary('Import File', required=True)

    def default_company(self):
        return Transaction().context.get('company', False)

ImportDatevInit()


class Import(Wizard):
    'Import'
    _name = 'account.import'

    def __init__(self):
        super(Import, self).__init__()
        self._error_messages.update({
            'missing_accounts': 'For the following datev codes no account or '
                'party could be found:\n\n%s',
            })
        self.states = copy.copy(self.states)
        self.states.update({
            'datev': {
                'result': {
                    'type': 'form',
                    'object': 'account.import.datev.init',
                    'state': [
                        ('end', 'Cancel', 'tryton-cancel'),
                        ('datev_import', 'Ok', 'tryton-ok', True),
                    ],
                },
            },
            'datev_import': {
                'result': {
                    'type': 'action',
                    'action': '_action_import_datev',
                    'state': 'end',
                },
            },
        })


    def _choice(self, data):
        return data['form']['type']

    def _action_import_datev(self, data):
        pool = Pool()
        batch_line_obj = pool.get('account.batch.line')
        account_obj = pool.get('account.account')
        party_obj = pool.get('party.party')
        period_obj = pool.get('account.period')
        model_data_obj = pool.get('ir.model.data')
        act_window_obj = pool.get('ir.action.act_window')

        file_data = str(data['form']['import_file_cache'])
        if HAS_CHARDET:
            enc = chardet.detect(file_data)['encoding']
            file_data = file_data.decode(enc)
        first_line = True
        import_lines = []
        accounts = set()
        sub_ledger_accounts = set()
        for line in file_data.split('\n'):
            if not line:
                continue
            if first_line:
                columns = [x for x in line.split(';')]
                length_columns = len(columns)
                first_line = False
            else:
                line_data = [x for x in line.split(';')]
                new_line = {}
                for i in range(length_columns-1):
                    new_line[columns[i]] = line_data[i]

                code = new_line['Gegenkonto']
                if len(code) > 4:
                    sub_ledger_accounts.add(code)
                else:
                    if len(code) < 4:
                        code = code
                        while len(code) < 4:
                            code = '0' + code
                    accounts.add(code)
                code = new_line['KtoNr']
                if len(code) > 4:
                    sub_ledger_accounts.add(code)
                else:
                    if len(code) < 4:
                        while len(code) < 4:
                            code = '0' + code
                    accounts.add(code)

                import_lines.append(new_line)

        period = period_obj.browse(data['form']['period'])
        account_values = account_obj.search_read([
            ('code', 'in', [x for x in accounts]),
            ('fiscalyear', '=', period.fiscalyear.id),
            ], fields_names=['code'])
        missing_accounts = []
        account_code2id = {}
        for value in account_values:
            account_code2id[value['code']] = value['id']
        for account in accounts:
            if account not in account_code2id:
                missing_accounts.append(account)
        sub_ledger_account_code2id = {}
        if sub_ledger_accounts:
            sub_ledger_account_values = party_obj.search_read(
                ['OR',
                    ('account_receivable_datev', 'in',
                        [x for x in sub_ledger_accounts]),
                    ('account_payable_datev', 'in',
                        [x for x in sub_ledger_accounts])
                ],
                fields_names=[
                    'account_receivable_datev',
                    'account_payable_datev',
                    ])
            for value in sub_ledger_account_values:
                sub_ledger_account_code2id[
                    value['account_receivable_datev']] = value['id']
                sub_ledger_account_code2id[
                    value['account_payable_datev']] = value['id']
        for account in sub_ledger_accounts:
            if account not in sub_ledger_account_code2id:
                missing_accounts.append(account)
        if missing_accounts:
            missing_accounts_string = ', '.join(
                [str(x) for x in missing_accounts])
            self.raise_user_error('missing_accounts',
                error_args=(missing_accounts_string,))

        new_ids = batch_line_obj.import_lines_datev(import_lines,
            data['form']['name_batch'], period)
        batch_line_obj.post(new_ids)

        model_data_ids = model_data_obj.search([
            ('fs_id', '=', 'act_batch_line_form'),
            ('module', '=', 'account_batch'),
            ('inherit', '=', False),
            ], limit=1)
        model_data = model_data_obj.browse(model_data_ids[0])
        res = act_window_obj.read(model_data.db_id)
        res['domain'] = str([
            ('id', 'in', new_ids),
            ])
        res['pyson_domain'] = PYSONEncoder().encode([('id', 'in', new_ids)])
        return res

Import()


class ExportDatev(ModelSQL, ModelView):
    _name = 'account.export.datev'

    include_imported_moves = fields.Boolean('Include imported moves',
        readonly=True)

ExportDatev()

class ExportDatevInit(ModelView):
    _name = 'account.export.datev.init'

    include_imported_moves = fields.Boolean('Include imported moves',
        states={
            'invisible': Equal(Eval('move'), 'no_export')
        }, depends=['move'])

ExportDatevInit()


class Export(Wizard):
    _name = 'account.export'

    def _get_export_vals(self, data):
        res = super(Export, self)._get_export_vals(data)
        res['include_imported_moves'] = data['form_init'][
            'include_imported_moves']
        return res

Export()

