# This file is part of Tryton. The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
import datetime
from decimal import Decimal
from trytond.model import ModelView, ModelSQL, fields
from trytond.transaction import Transaction
from trytond.pool import Pool
from trytond.wizard import Wizard


class BatchLine(ModelSQL, ModelView):
    _name = 'account.batch.line'

    imported = fields.Boolean('Imported', readonly=True)

    def __init__(self):
        super(BatchLine, self).__init__()
        self._error_messages.update({
            'missing_account': 'Can not find account for code %s. '
                'Please create it.',
            'field_not_supported': 'The field "%s" is not supported by this '
                'import API!',
            'different_vat_code': 'The provided VAT Code %s is different to '
                'the recorded VAT Code %s on the provided party %s.',
            'tax_not_found': 'Could not find tax for tax key:\nTax Key: %s\n'
                'VAT Fact: %s',
            'vat_code_multiple_match': 'Found more than one party with the '
                'VAT Code %s. Can not determine the correct one.',
            'party_not_found': 'Can not find party for VAT Code %s.',
            'missing_vat_code': 'Needed VAT Code not given.',
            })

    def create_move_dict(self, batch_line_vals):
        res = super(BatchLine, self).create_move_dict(batch_line_vals)
        if batch_line_vals.get('imported'):
            res['imported'] = True
        return res

    def import_lines_datev(self, data, batch_name, period):
        pool = Pool()
        account_obj = pool.get('account.account')
        account_split_obj = pool.get('account.account.split')
        batch_journal_obj = pool.get('account.batch.journal')
        batch_obj = pool.get('account.batch')
        period_obj = pool.get('account.period')
        party_obj = pool.get('party.party')
        tax_obj = pool.get('account.tax')

        if isinstance(period, (int, long)):
            period = period_obj.browse(period)
        fiscalyear = period.fiscalyear
        journal_id = batch_journal_obj.search(
            [('name', '=', 'DATEV Import')])[0]
        batch_id = batch_obj.create({
            'name': batch_name,
            'journal': journal_id,
            })

        new_ids = []
        for record in data:
            party_id = False
            account_id = False
            contra_account_id = False
            date = False
            amount = False
            tax_id = False
            reversal = False
            split = False
            maturity_date = False

            year = period.end_date.year
            date = datetime.date(year, int(record['Belegdatum'][-2:]),
                int(record['Belegdatum'][:2]))
            if record['Beleg2']:
                try:
                    maturity_date = datetime.date(year,
                        int(record['Beleg2'][-2:]), int(record['Beleg2'][:2]))
                except:
                    pass

            if len(record['KtoNr']) > 4 and len(record['Gegenkonto']) > 4:
                split = True
            account_id, party_id = account_obj.get_account_party_by_datev_code(
                record['KtoNr'], date=date, fiscalyear=fiscalyear)
            if not account_id:
                self.raise_user_error('missing_account',
                    error_args=(record['KtoNr']))
            contra_account_id, contra_party_id = (
                account_obj.get_account_party_by_datev_code(
                    record['Gegenkonto'], date=date, fiscalyear=fiscalyear))
            if not contra_account_id:
                self.raise_user_error('missing_account',
                    error_args=(record['Gegenkonto']))

            if not split:
                party_id = party_id or contra_party_id

            for field in ['Skonto', 'EUSteuersatz']:
                if record.get(field, '0,00') != '0,00':
                    self.raise_user_error('field_not_supported',
                        error_args=(field,))

            if record.get('EULand_UstID'):
                vat_code_vals = party_obj.read(party_id,
                    fields_names=['vat_code', 'rec_name'])
                if record['EULand_UstID'] != vat_code_vals['vat_code']:
                    self.raise_user_error('different_vat_code',
                        error_args=(record['EULand_UstID'],
                            vat_code_vals['vat_code'],
                            vat_code_vals['rec_name']))

            amount = Decimal(
                record['EingegUmsatz'].replace('.', '').replace(',', '.'))
            if record['SollHaben'] == 'S':
                amount *= -1

            with Transaction().set_context(effective_date=date):
                if record['BUFeld'].strip():
                    vat_fact_13b = False
                    if record.get('InformationsArt1', '') == 'D_L+L-Art':
                        vat_fact_13b = record.get('Informationsinhalt1', False)
                        if vat_fact_13b:
                            vat_fact_13b, _ = vat_fact_13b.split('_')
                    correction_key = record['BUFeld'][:1]
                    correction_key = correction_key.replace(' ', '')
                    if not correction_key:
                        correction_key = '0'
                    tax_key = record['BUFeld'][-1:]
                    if tax_key == '0' and correction_key == '2':
                        reversal = True
                    elif tax_key != '0':
                        args = [
                                ['OR',
                                    ('correction_key', '=', correction_key),
                                    ('reverse_correction_key', '=',
                                        correction_key)
                                ],
                                ('tax_key', '=', tax_key),
                                ('vat_fact_13b', '=', vat_fact_13b)
                            ]
                        tax_ids = tax_obj.search(args)
                        if tax_ids:
                            tax_id = tax_ids[0]
                        else:
                            self.raise_user_error('tax_not_found',
                                error_args=(correction_key + tax_key,
                                    vat_fact_13b or 'False'))
                        account = account_obj.browse(account_id)
                        contra_account = account_obj.browse(contra_account_id)
                        tax = tax_obj.browse(tax_id)
                        if tax.vat_code_required and not party_id:
                            if record.get('EULand_UstID'):
                                party_ids = party_obj.search([
                                    ('vat_code', '=', record['EULand_UstID'])])
                                if party_ids:
                                    if len(party_ids) != 1:
                                        self.raise_user_error(
                                            'vat_code_multiple_match',
                                            error_args=(record['EULand_UstID'],)
                                            )
                                    else:
                                        party_id = party_ids[0]
                                else:
                                    self.raise_user_error('party_not_found',
                                        error_args=(record['EULand_UstID'],))
                            else:
                                self.raise_user_error('missing_vat_code')
                        if (contra_account.account_datev.additional_function and
                                contra_account.account_datev.additional_function
                                != tax.tax_function_type):
                            if (not account.account_datev.additional_function or
                                    account.account_datev.additional_function ==
                                    tax.tax_function_type):
                                old_account_id = account_id
                                account_id = contra_account_id
                                contra_account_id = old_account_id
                                amount *= -1

                vals = {
                    'posting_text': record['BuchText'],
                    'batch': batch_id,
                    'journal': journal_id,
                    'fiscalyear': fiscalyear.id,
                    'date': date,
                    'amount': Decimal(amount),
                    'account': account_id,
                    'tax': tax_id,
                    'contra_account': contra_account_id,
                    'party': party_id,
                    'external_reference': record['Beleg1'],
                    'is_cancelation_move': reversal,
                    'maturity_date': maturity_date,
                    'imported': True,
                    }
                if split:
                    vals2 = vals.copy()
                    vals2['account'] = account_split_obj.get_split_account(
                        date=date, fiscalyear=fiscalyear)
                    vals2['party'] = contra_party_id
                    new_id = self.create(vals2)
                    new_ids.append(new_id)

                    vals2 = vals.copy()
                    vals2['contra_account'] = (
                        account_split_obj.get_split_account(date=date,
                            fiscalyear=fiscalyear))
                    vals2['party'] = party_id
                    new_id = self.create(vals2)
                    new_ids.append(new_id)
                else:
                    new_id = self.create(vals)
                    new_ids.append(new_id)
        return new_ids

BatchLine()


class CancelBatchLinesAskImported(ModelView):
    'Cancel Batch Lines Ask Imported'
    _name = 'account.batch.cancel.ask.imported'
    _description = __doc__

    export_behavior = fields.Selection([
        ('default', 'Include in default export'),
        ('imported', 'Include only in export with imported moves')
        ], 'Export Behavior', required=True)

CancelBatchLinesAskImported()


class CancelBatchLines(Wizard):
    _name = 'account.batch.cancel'

    def __init__(self):
        super(CancelBatchLines, self).__init__()
        self.states.update({
            'ask_imported': {
                'result': {
                    'type': 'form',
                    'object': 'account.batch.cancel.ask.imported',
                    'state': [
                        ('end', 'No', 'tryton-cancel'),
                        ('cancelation', 'Yes', 'tryton-ok', True),
                    ]
                },
            },
        })

    def _check_cancelation(self, data):
        batch_line_obj = Pool().get('account.batch.line')
        batch_line_ids = batch_line_obj.search([
            ('imported', '=', True),
            ('id', 'in', data['ids']),
            ])
        if batch_line_ids:
            return 'ask_imported'
        return super(CancelBatchLines, self)._check_cancelation(data)

    def _process_cancelation_values(self, batch_line_id,
            export_behavior='default'):
        batch_line_obj = Pool().get('account.batch.line')

        res = super(CancelBatchLines, self)._process_cancelation_values(
            batch_line_id)
        batch_line = batch_line_obj.browse(batch_line_id)
        if export_behavior == 'imported':
            res['imported'] = batch_line.imported
        return res

    def _cancel_post(self, data):
        batch_line_obj = Pool().get('account.batch.line')

        export_behavior = data['form'].get('export_behavior', 'default')
        for batch_line in batch_line_obj.browse(data['ids']):
            if batch_line.state != 'posted':
                continue
            cancelation_values = self._process_cancelation_values(
                batch_line.id, export_behavior=export_behavior)
            cancel_batch_line_id = batch_line_obj.create(cancelation_values)
            batch_line_obj.post(cancel_batch_line_id)
        return {}

CancelBatchLines()
