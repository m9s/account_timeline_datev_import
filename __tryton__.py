# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
{
    'name': 'Account Timeline Datev Import',
    'name_de_DE': 'Buchhaltung Gültigkeitsdauer Datev Import',
    'version': '2.2.0',
    'author': 'virtual things',
    'email': 'info@virtual-things.biz',
    'website': 'http://www.virtual-things.biz',
    'description': '''Accounting module providing Datev functionality
    - Provides the possbility to import Datev exports
    ''',
    'description_de_DE': '''Buchhaltungsmodul für Datevfunktionalität
    - Ermöglicht den Import von Datev Exporten
    ''',
    'depends': [
        'account_batch_invoice_timeline',
        'account_batch_timeline',
        'account_batch_tax_timeline',
        'account_import',
        'account_timeline_datev',
    ],
    'xml': [
        'account.xml',
        'batch.xml',
        'datev.xml',
        'journal.xml',
        'move.xml',
    ],
    'translation': [
        'locale/de_DE.po',
    ],
}
