# This file is part of Tryton. The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
import datetime
from trytond.model import ModelView, ModelSQL, fields
from trytond.pyson import Eval, If, In, Get
from trytond.transaction import Transaction
from trytond.pool import Pool


class Account(ModelSQL, ModelView):
    _name = 'account.account'

    def __init__(self):
        super(Account, self).__init__()
        self._error_messages.update({
            'datev_code_multiple_match': 'Found more than one party with the '
                'Datev Code %s. Can not determine the correct one.',
            })

    def get_account_party_by_datev_code(self, code, date=None, fiscalyear=None):
        account_obj = Pool().get('account.account')
        party_obj = Pool().get('party.party')

        with Transaction().set_context(effective_date=date):
            party_id = False
            account_id = False
            if len(code) > 4:
                party_ids = party_obj.search(['OR',
                    ('account_payable_datev', '=', code),
                    ('account_receivable_datev', '=', code),
                    ])
                if party_ids:
                    if len(party_ids) == 1:
                        party_id = party_ids[0]
                        party = party_obj.browse(party_id)
                        if party.account_payable_datev == code:
                            account_id = party.account_payable.id
                        elif party.account_receivable_datev == code:
                            account_id = party.account_receivable.id
                    else:
                        self.raise_user_error('datev_code_multiple_match',
                            error_args=(code))
            else:
                while len(code) < 4:
                    code = '0' + code
                account_ids = account_obj.search([
                    ('code', '=', code),
                    ('fiscalyear', '=', fiscalyear)
                    ])
                if account_ids:
                    account_id = account_ids[0]
            return account_id, party_id

Account()


class SplitAccount(ModelSQL, ModelView):
    'Split Account'
    _name = 'account.account.split'
    _description = __doc__

    def __init__(self):
        super(SplitAccount, self).__init__()
        self._constraints += [
            ('check_one_account_fiscalyear', 'one_account_per_fiscalyear'),
        ]
        self._error_messages.update({
            'one_account_per_fiscalyear': 'There can be only one account ' \
                    'per company and fiscalyear!',
            'no_split_account': 'Could not find split account for fiscalyear '
                    '%s!'
            })

    account = fields.Many2One('account.account', 'Account', required=True,
        domain=[
            ('fiscalyear', '=', Eval('fiscalyear')),
            ('company', '=', Eval('company')),
            ('kind', '!=', 'view'),
            ], depends=['fiscalyear', 'company'])
    company = fields.Many2One('company.company', 'Company',
        domain=[
            ('id', If(In('company', Eval('context', {})), '=', '!='),
                Get(Eval('context', {}), 'company', 0)),
        ], required=True)
    fiscalyear = fields.Many2One('account.fiscalyear', 'Fiscalyear',
        domain=[('company', '=', Eval('company'))], required=True,
        depends=['company'])

    def default_company(self):
        return Transaction().context.get('company') or False

    def get_split_account(self, date=None, fiscalyear=None):
        account_obj = Pool().get('account.account')
        fiscalyear_obj = Pool().get('account.fiscalyear')

        if not fiscalyear:
            if not date:
                date = datetime.date.today()
            fiscalyear = fiscalyear_obj.get_fiscalyear(date=date)
        account_values = self.search_read([
            ('fiscalyear', '=', fiscalyear.id),
            ], limit=1, fields_names=['account'])
        if account_values:
            return account_obj.browse(account_values['account'])
        self.raise_user_error('no_split_account', error_args=(fiscalyear.name,))

    def check_one_account_fiscalyear(self, ids):
        account_ids = self.search([])
        accounts = self.browse(account_ids)
        accounts_company_fiscalyear = {}
        res = True
        for account in accounts:
            company_id = account.company.id
            fiscalyear_id = account.fiscalyear.id
            accounts_company_fiscalyear.setdefault(company_id, {})
            accounts_company_fiscalyear[company_id].setdefault(
                fiscalyear_id, [])
            accounts_company_fiscalyear[company_id][fiscalyear_id].append(
                account.id)
            if len(accounts_company_fiscalyear[company_id][
                    fiscalyear_id]) > 1:
                res = False
                break
        return res

SplitAccount()
